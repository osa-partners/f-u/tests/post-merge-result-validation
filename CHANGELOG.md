# [0.17.0](https://gitlab.com/openstapps/core/compare/v0.16.0...v0.17.0) (2019-04-16)



# [0.16.0](https://gitlab.com/openstapps/core/compare/v0.15.0...v0.16.0) (2019-04-15)



# [0.15.0](https://gitlab.com/openstapps/core/compare/v0.14.0...v0.15.0) (2019-04-09)


### Bug Fixes

* change SCThingMeta getInstance() return value ([4986042](https://gitlab.com/openstapps/core/commit/4986042))
* resolve issues with things that can be offered ([623ed61](https://gitlab.com/openstapps/core/commit/623ed61)), closes [#41](https://gitlab.com/openstapps/core/issues/41)


### Features

* provide context based search ([3242411](https://gitlab.com/openstapps/core/commit/3242411))



# [0.14.0](https://gitlab.com/openstapps/core/compare/v0.13.0...v0.14.0) (2019-04-03)


### Features

* add model for plugin register route ([8188731](https://gitlab.com/openstapps/core/commit/8188731))



# [0.13.0](https://gitlab.com/openstapps/core/compare/v0.12.0...v0.13.0) (2019-04-02)


### Bug Fixes

* correct isThing guard ([67868e9](https://gitlab.com/openstapps/core/commit/67868e9))
* update tslint  dependencies ([bbe4fca](https://gitlab.com/openstapps/core/commit/bbe4fca))


### Features

* add conditional "maps" for associated types ([c8bda2e](https://gitlab.com/openstapps/core/commit/c8bda2e)), closes [#50](https://gitlab.com/openstapps/core/issues/50)
* provide sample JSON files with the package ([5d1e79d](https://gitlab.com/openstapps/core/commit/5d1e79d)), closes [#46](https://gitlab.com/openstapps/core/issues/46)



# [0.12.0](https://gitlab.com/openstapps/core/compare/v0.11.0...v0.12.0) (2019-03-14)


### Bug Fixes

* add todo to SCThingsWithoutDiff and SCClasses ([9a49442](https://gitlab.com/openstapps/core/commit/9a49442)), closes [#39](https://gitlab.com/openstapps/core/issues/39)


### Features

* add SCThingTranslator class. move functionality accordingly ([90e3d22](https://gitlab.com/openstapps/core/commit/90e3d22))



# [0.11.0](https://gitlab.com/openstapps/core/compare/v0.10.0...v0.11.0) (2019-02-21)


### Features

* add laboratory and computer as room categories ([a0ab72e](https://gitlab.com/openstapps/core/commit/a0ab72e)), closes [#33](https://gitlab.com/openstapps/core/issues/33)
* add maxRequestBodySize in backend configuration ([b5bd09e](https://gitlab.com/openstapps/core/commit/b5bd09e))



# [0.10.0](https://gitlab.com/openstapps/core/compare/v0.9.0...v0.10.0) (2019-02-18)


### Features

* add model for requestBodyTooLargeError ([bc3a0f6](https://gitlab.com/openstapps/core/commit/bc3a0f6))



# [0.9.0](https://gitlab.com/openstapps/core/compare/v0.8.0...v0.9.0) (2019-02-14)



# [0.8.0](https://gitlab.com/openstapps/core/compare/v0.7.0...v0.8.0) (2019-02-13)


### Features

* add config for maximum queries ([c7ab473](https://gitlab.com/openstapps/core/commit/c7ab473))



# [0.7.0](https://gitlab.com/openstapps/core/compare/v0.6.0...v0.7.0) (2019-02-13)



# [0.6.0](https://gitlab.com/openstapps/core/compare/v0.5.0...v0.6.0) (2019-02-07)



# [0.5.0](https://gitlab.com/openstapps/core/compare/v0.4.0...v0.5.0) (2019-02-06)


### Features

* add model for syntax error ([a3f9fcb](https://gitlab.com/openstapps/core/commit/a3f9fcb))



# [0.4.0](https://gitlab.com/openstapps/core/compare/v0.3.0...v0.4.0) (2019-01-31)


### Features

* add draft of todo ([2860a11](https://gitlab.com/openstapps/core/commit/2860a11))



# [0.3.0](https://gitlab.com/openstapps/core/compare/v0.2.0...v0.3.0) (2019-01-25)


### Bug Fixes

* set larger v8 stack size ([d3d08e7](https://gitlab.com/openstapps/core/commit/d3d08e7))


### Features

* add different origin types: remote and user ([13a4965](https://gitlab.com/openstapps/core/commit/13a4965)), closes [#12](https://gitlab.com/openstapps/core/issues/12)
* add saveable thing for saving user/client data ([a4f3fab](https://gitlab.com/openstapps/core/commit/a4f3fab)), closes [#12](https://gitlab.com/openstapps/core/issues/12)



# [0.2.0](https://gitlab.com/openstapps/core/compare/v0.1.0...v0.2.0) (2019-01-09)


### Features

* use tag [@validatable](https://gitlab.com/validatable) to mark schema types ([7f248ee](https://gitlab.com/openstapps/core/commit/7f248ee))



# [0.1.0](https://gitlab.com/openstapps/core/compare/v0.0.2...v0.1.0) (2018-12-17)


### Features

* add base scheme for academic degrees ([85c8fc4](https://gitlab.com/openstapps/core/commit/85c8fc4))
* add schema for course of studies ([2d4a76a](https://gitlab.com/openstapps/core/commit/2d4a76a))
* add tool to generate documentation for routes ([1a07df2](https://gitlab.com/openstapps/core/commit/1a07df2))



## [0.0.2](https://gitlab.com/openstapps/core/compare/v0.0.1...v0.0.2) (2018-11-29)



## [0.0.1](https://gitlab.com/openstapps/core/compare/2d770dd...v0.0.1) (2018-11-29)


### Features

* add core ([2d770dd](https://gitlab.com/openstapps/core/commit/2d770dd))



