/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAbstractRoute, SCRouteHttpVerbs} from '../../../Route';
import {SCThingType} from '../../../Thing';
import {SCISO8601Date} from '../../../types/Time';
import {
  SCInternalServerErrorResponse,
  SCMethodNotAllowedErrorResponse,
  SCRequestBodyTooLargeErrorResponse,
  SCSyntaxErrorResponse,
  SCUnsupportedMediaTypeErrorResponse,
  SCValidationErrorResponse,
} from '../../errors/ErrorResponse';

/**
 * A bulk request
 *
 * Parameters to be sent to request a new bulk.
 *
 * @validatable
 */
export interface SCBulkRequest extends SCBulkParameters {
}

/**
 * Parameters for a bulk
 */
export interface SCBulkParameters {
  /**
   * Expiration of bulk
   *
   * If the date is hit and the bulk is not done, it will be deleted and all data removed.
   * Defaults to one hour.
   */
  expiration?: SCISO8601Date;

  /**
   * Source of data for this bulk
   *
   * A short "description" of the source of the data to identify it inside the database.
   * A second bulk with the same source overrides the data of the first bulk once it is done.
   */
  source: string;

  /**
   * Type of things that are indexed in this bulk.
   * 
   */
  type: SCThingType;
}

/**
 * Route for bulk creation
 */
export class SCBulkRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCBulkRequest';
    this.responseBodyName = 'SCBulkResponse';
    this.statusCodeSuccess = 200;
    this.urlFragment = '/bulk';
  }
}
