/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCAppConfiguration} from '../types/config/App';
import {SCBackendConfiguration, SCBackendInternalConfiguration} from '../types/config/Backend';
import {SCLicensePlate} from '../types/namespaces';

/**
 * A configuration file that configures app and backend
 * @validatable
 */
export interface SCConfigFile {

  /**
   * Configuration for the app that is visible to clients
   */
  app: SCAppConfiguration;

  /**
   * Configuration for the backend that is visible to clients
   */
  backend: SCBackendConfiguration;

  /**
   * Configuration that is not visible to clients
   */
  internal: SCBackendInternalConfiguration;

  /**
   * UID of the university, e.g. the license plate
   */
  uid: SCLicensePlate;
}
