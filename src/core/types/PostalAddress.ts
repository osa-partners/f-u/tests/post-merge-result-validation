/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * A postal address
 */
export interface SCPostalAddress {
  /**
   * Country of the address
   */
  addressCountry: string;

  /**
   * City of the address
   */
  addressLocality: string;

  /**
   * State of the address
   */
  addressRegion?: string;

  /**
   * Zip code of the address
   */
  postalCode: string;

  /**
   * Optional post box number
   */
  postOfficeBoxNumber?: string;

  /**
   * Street of the address - with house number!
   */
  streetAddress: string;
}
