/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCBuildingWithoutReferences} from '../things/Building';
import {SCPointOfInterestWithoutReferences} from '../things/PointOfInterest';
import {SCRoomWithoutReferences} from '../things/Room';
/**
 * Something that is or happens in a place
 * 
 * !Important!
 * This is not a SCThing.
 */
export interface SCInPlace {
  /**
   * Place the thing is or happens in
   */
  inPlace?:
    SCBuildingWithoutReferences
    | SCPointOfInterestWithoutReferences
    | SCRoomWithoutReferences;
}
