/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';
import {SCInPlace} from '../types/Places';

/**
 * A thing that is or happens in a place
 */
export interface SCThingInPlace extends SCThing, SCInPlace {}

/**
 * Meta information about thing in a place
 */
export class SCThingInPlaceMeta extends SCThingMeta implements SCMetaTranslations<SCThingInPlace> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
      inPlace: 'Ort',
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
      inPlace: 'location',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
