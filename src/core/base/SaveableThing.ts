/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingUserOrigin} from '../Thing';

/**
 * An encapsulation of the data (e.g. a thing) that is saved, which provides additional information.
 */
export interface SCSaveableThingWithoutReferences extends SCThing {
  /**
   * Type of the origin
   */
  origin: SCThingUserOrigin;
}

/**
 * An encapsulation of the data (e.g. a thing) that is saved, which provides additional information.
 */
export interface SCSaveableThing<T extends SCThing> extends SCSaveableThingWithoutReferences {
  /**
   * The contained data
   */
  data: T;
}
