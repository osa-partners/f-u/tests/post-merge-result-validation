/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingInPlace, SCThingInPlaceMeta} from '../base/ThingInPlace';
import {SCThing, SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations} from '../types/i18n';
import {SCISO8601Duration} from '../types/Time';

/**
 * A ticket without references
 */
export interface SCTicketWithoutReferences extends SCThing {
  /**
   * Approximate wait time
   */
  approxWaitingTime: SCISO8601Duration;

  /**
   * Waiting number of the ticket
   */
  currentTicketNumber: string;

  /**
   * Service type of the ticket
   */
  serviceType: any;

  /**
   * Type of a ticket
   */
  type: SCThingType.Ticket;
}

/**
 * A ticket
 *
 * @validatable
 */
export interface SCTicket extends SCTicketWithoutReferences, SCThingInPlace {
  /**
   * Type of a ticket
   */
  type: SCThingType.Ticket;
}

/**
 * Meta information about a ticket
 */
export class SCTicketMeta extends SCThingMeta implements SCMetaTranslations<SCTicket> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.de,
      type: 'Ticket',
    },
    en: {
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Ticket,
    },
  };
}
