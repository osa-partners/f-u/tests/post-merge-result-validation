/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Feature, FeatureCollection, GeometryObject, LineString} from 'geojson';
import {SCThingInPlace, SCThingInPlaceMeta} from '../base/ThingInPlace';
import {SCThing, SCThingMeta, SCThingTranslatableProperties, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCPointOfInterestWithoutReferences} from './PointOfInterest';
import {SCRoomWithoutReferences} from './Room';

/**
 * A floor without references
 */
export interface SCFloorWithoutReferences extends SCThing {
  /**
   * Floor name in the place it is in e.g. "first floor", "ground floor". This doesn't reference the building name.
   */
  floorName: string;

  /**
   * Floor plan
   */
  plan: SCFloorFeatureCollectionWithPlaces<LineString, any>;

  /**
   * Translated fields of a floor
   */
  translations?: SCTranslations<SCFloorTranslatableProperties>;

  /**
   * Type of a floor
   */
  type: SCThingType.Floor;
}

/**
 * A floor
 *
 * @validatable
 */
export interface SCFloor extends SCFloorWithoutReferences, SCThingInPlace {
  /**
   * Translated fields of a floor
   */
  translations?: SCTranslations<SCFloorTranslatableProperties>;

  /**
   * Type of a floor
   */
  type: SCThingType.Floor;
}

/**
 * A feature collection
 */
export interface SCFloorFeatureCollectionWithPlaces<T extends GeometryObject, P = any>
  extends FeatureCollection<T, P> {
  /**
   * Features of the collection
   */
  features: Array<SCFloorFeatureWithPlace<T, P>>;
}

/***
 * A feature with a place
 */
export interface SCFloorFeatureWithPlace<T extends GeometryObject, P = any>
  extends Feature<T, P> {
  /**
   * The place of the feature
   */
  place?: SCRoomWithoutReferences | SCPointOfInterestWithoutReferences;
}

/**
 * Translatable properties of a floor
 */
export interface SCFloorTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * Translation of the floor name
   */
  floorName?: string;
}

/**
 * Meta information about floors
 */
export class SCFloorMeta extends SCThingMeta implements SCMetaTranslations<SCFloor> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingInPlaceMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.de,
      type: 'Etage',
    },
    en: {
      ... SCThingInPlaceMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Floor,
    },
  };
}
