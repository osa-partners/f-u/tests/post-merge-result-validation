/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from '../base/CreativeWork';
import {SCThingThatCanBeOfferedTranslatableProperties} from '../base/ThingThatCanBeOffered';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';

/**
 * A message without references
 */
export interface SCMessageWithoutReferences extends SCCreativeWorkWithoutReferences {
  /**
   * Audience of the message
   */
  audiences: SCMessageAudience[];

  /**
   * When the message was created
   */
  dateCreated?: SCISO8601Date;

  /**
   * Message itself
   */
  message: string;

  /**
   * Translated fields of a message
   */
  translations?: SCTranslations<SCMessageTranslatableProperties>;

  /**
   * Type of a message
   */
  type: SCThingType.Message;
}

/**
 * A message
 *
 * @validatable
 */
export interface SCMessage extends SCCreativeWork, SCMessageWithoutReferences {
  /**
   * Translated fields of a message
   */
  translations?: SCTranslations<SCMessageTranslatableProperties>;

  /**
   * Type of a message
   */
  type: SCThingType.Message;
}

/**
 * Audiences for messages
 */
export type SCMessageAudience =
  'students'
  | 'employees'
  | 'guests';

/**
 * Translatable properties of a message
 */
export interface SCMessageTranslatableProperties
  extends SCCreativeWorkTranslatableProperties, SCThingThatCanBeOfferedTranslatableProperties {
  /**
   * Message itself
   */
  message?: string;
}

/**
 * Meta information about messages
 */
export class SCMessageMeta extends SCThingMeta implements SCMetaTranslations<SCMessage> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.de,
      type: 'Nachricht',
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Message,
    },
  };
}
