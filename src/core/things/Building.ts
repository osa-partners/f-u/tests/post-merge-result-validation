/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCPlaceWithoutReferences,
  SCPlaceWithoutReferencesMeta,
  SCPlaceWithoutReferencesTranslatableProperties,
} from '../base/Place';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';

export type SCBuildingCategories =
  'cafe'
  | 'education'
  | 'library'
  | 'office'
  | 'canteen'
  | 'student canteen'
  | 'restaurant'
  | 'restroom';

export interface SCBuildingWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCBuildingCategories,
    SCThingWithCategoriesSpecificValues>,
    SCPlaceWithoutReferences {
  /**
   * Categories of a building
   */
  categories: SCBuildingCategories[];

  /**
   * List of floor names of the place
   */
  floors?: string[];

  /**
   * Translated fields of a building
   */
  translations?: SCTranslations<SCBuildingTranslatableProperties>;

  /**
   * Type of the building
   */
  type: SCThingType.Building;
}

/**
 * A building
 *
 * @validatable
 */
export interface SCBuilding extends SCBuildingWithoutReferences {
  /**
   * Translated fields of a building
   */
  translations?: SCTranslations<SCBuildingTranslatableProperties>;

  /**
   * Type of the building
   */
  type: SCThingType.Building;
}

export interface SCBuildingTranslatableProperties
       extends SCPlaceWithoutReferencesTranslatableProperties, SCThingWithCategoriesTranslatableProperties {
  floors?: string[];
}

  /**
   * Meta information about a place
   */
export class SCBuildingMeta extends SCThingMeta implements SCMetaTranslations<SCBuilding> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCBuildingCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.de,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.de,
      floors: 'Etagen',
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCBuildingCategories,
            SCThingWithCategoriesSpecificValues>().fieldTranslations.en,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCBuildingCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.de,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.de,
      categories: {
        'cafe': 'Café',
        'canteen': 'Kantine',
        'education': 'Bildung',
        'library': 'Bibliothek',
        'office': 'Büro',
        'restaurant': 'Restaurant',
        'restroom': 'Toilette',
        'student canteen': 'Mensa',
      },
      type: 'Gebäude',
    },
    en: {
      ... SCThingWithCategoriesWithoutReferencesMeta.getInstance<SCBuildingCategories,
            SCThingWithCategoriesSpecificValues>().fieldValueTranslations.en,
      ... SCPlaceWithoutReferencesMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Building,
    },
  };
}
